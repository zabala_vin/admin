admin.controller('newOfferModal', function ($scope, $uibModal, $log) {

  $scope.open = function () {

    var modalInstance = $uibModal.open({
      templateUrl: 'newOffer.html',
      controller: 'newOfferInstance'
    });

    modalInstance.result.then(function (selectedItem) {
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
});

admin.controller('newOfferInstance', function ($scope, $state, $uibModalInstance) {

    $scope.ok = function () {

        //$uibModalInstance.close($scope.selected.item);
        $uibModalInstance.dismiss('cancel');
    };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});