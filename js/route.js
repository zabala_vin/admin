admin.config(function($stateProvider, $urlRouterProvider) {
  
  $urlRouterProvider.otherwise("dashboard")
  
  $stateProvider
	  .state('dashboard', {
	  	url: '/dashboard',
	    templateUrl: 'views/dashboard.html',
	    controller: 'mainController',
	    authenticate: false
  		})
	  
	  .state('offers', {
	  	url: '/offers',
	    templateUrl: 'views/offers.html',
	    controller: '',
	    authenticate: true
		})

	  .state('orders', {
	  	url: '/orders',
	    templateUrl: 'views/orders.html',
	    controller: '',
	    authenticate: true
		});
});