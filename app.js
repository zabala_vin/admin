var dependencies = [
  'ui.bootstrap', 
  'ui.bootstrap.modal',
  'ui.router'
];

var admin = angular.module('admin', dependencies);

admin.controller('mainController', function($scope, $state) {

	$scope.isCollapsed = true;
	$scope.newCategorie = true;

	$state.transitionTo('dashboard');
});